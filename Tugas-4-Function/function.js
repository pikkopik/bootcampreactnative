//No. 1 
//Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.

console.log(' ')
console.log('=============== Soal 1 ===============')
console.log(' ')

function teriak() {
    var kata = 'Halo Sanbers!'
    return kata
}
console.log(teriak())

//No. 2 
//Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.

console.log(' ')
console.log('=============== Soal 2 ===============')
console.log(' ')

function kalikan(num1, num2) {
    return num1 * num2
}
var num1 = 12
var num2 = 4
var Hasilkali = kalikan(num1, num2)
console.log(Hasilkali)

//Soal 3
//Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”

console.log(' ')
console.log('=============== Soal 3 ===============')
console.log(' ')

function introduce(name, age, address, hobby) {
    var perkenalkan = 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!'
    return perkenalkan
}
var name = 'Agus'
var age = 30
var address = 'Jln. Malioboro, Yogyakarta'
var hobby = 'Gaming'
var perkenalan  = introduce(name, age, address, hobby)
console.log(perkenalan )
console.log(' ')