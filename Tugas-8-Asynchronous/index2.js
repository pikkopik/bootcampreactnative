var readbookspromise = require('./promise.js')

var book = [
    { name: 'LOTR', timespent: 3000 },
    { name: 'Fidas', timespent: 2000 },
    { name: 'Kalkulus', timespent: 4000 }
]



async function asynccall() {

    let t = 10000
    for (let i = 0; i < book.length; i++) {
        t = await readbookspromise(t, book[i]).then(function (sisawaktu) {
            return sisawaktu;
        })
            .catch(function (sisawaktu) {
                return sisawaktu;
            })
    }
    console.log('selesai')
}
asynccall()
