import React from 'react';
import { View, Text, TextInput, StyleSheet, Button,Image,TouchableOpacity} from 'react-native';
import HomeScreen from './HomeScreen'
import { SplashBackground, Logo } from '../../assets'
export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
    var password=this.state.password
    
    if(password==12345678){
      console.log('login berhasil')
      this.setState({isError:false})
      this.props.navigation.navigate('Home',{ 
      userName:this.state.userName})
      
    }
    else{
      console.log('Password salah')
      this.setState({isError:true})
    }
  }

  render() {
    
    return (
      <View style={styles.container}>
        <View style={{paddingBottom:10}}>
        <Image source={Logo} style={styles.logo} />
          <Text style={styles.titleText}>Sign In</Text>
          
        </View>

        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            {/* <MaterialCommunityIcons name='account-circle' color='blue' size={40} /> */}
            <View>
              <Text style={styles.labelText}>Username/Email</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Nama User/Email'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            {/* <MaterialCommunityIcons name='lock' color='blue' size={40} /> */}
            <View>
              <Text style={styles.labelText}>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
          <Button title='Login' onPress={() => this.loginHandler()} color={'red'} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4A0004'
  },
  titleText: {
    fontSize: 48,
    fontWeight: 'bold',
    color: '#FFCCCC',
    textAlign: 'center',
  },
  subTitleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'blue',
    alignSelf: 'flex-end',
    marginBottom: 16
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold',
    color: '#FFCCCC'
  },
  textInput: {
    width: 300,
    backgroundColor: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
  logo : {
    width: 222,
    height: 105
}
});
