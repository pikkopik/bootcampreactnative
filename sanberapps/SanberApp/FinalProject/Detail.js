import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, View , Image,Dimensions,TouchableOpacity,Button,FlatList} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';


const Detail = ({navigation,route}) => {
   const DEVICE = Dimensions.get('window');
    const {detail}=route.params
    
    return (
        <View style={styles.container} >
          <View style={{ paddingTop:5,paddingLeft:5,backgroundColor: 'white',flexDirection:'row'}}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Icon name='arrow-back' size={40} color={'#800020'} />
            </TouchableOpacity>
            <TouchableOpacity style={{ paddingLeft:300}} >
             <Icon name='md-heart-outline' size={35} color={'#800020'} />
            </TouchableOpacity>
          </View>
          <ScrollView>
          <View style={{
              backgroundColor: 'white',
              minHeight:320,
              width:DEVICE.width,
              alignItems: 'center',
              justifyContent:'space-around',
              flexDirection:'row',
            }}>  
            <Image
              source={{ uri: detail.image }}
              style={styles.itemImage}
            ></Image>
          </View>
          <View style={{flex:1}}>
            <View style={{
                paddingLeft:10,
              }}>
            <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
              {detail.title}
            </Text>
            <Text style={styles.itemPrice}>
              ${detail.price}
            </Text>
            <Text >Category: {detail.category }</Text>
            </View>
            <View style={styles.itemDesc}>
              <Text style={{fontWeight: 'bold',paddingLeft:10,}}>Description</Text>
              <Text style={{
                padding:10,
              }}>
              {detail.description}
            </Text>
            </View>
          </View>
          </ScrollView>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between' 
          }}>
          
            <TouchableOpacity style={{ margin: 8}} >
             <Icon name='chatbox-ellipses' size={40} color={'#800020'} />
            </TouchableOpacity>
            <View style={{ marginTop: 10,width:240}}>
            <Button title='Buy Now' color={'#800020'} />
            </View>
            <TouchableOpacity style={{ margin: 8}} >
             <Icon name='cart' size={40} color={'#800020'} />
            </TouchableOpacity>
            </View>
            
      </View>
      
    )
    
}


export default Detail

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    itemContainer: {
      width: 200,
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#FFFFFF',
      marginHorizontal: 5,
      marginVertical: 5,
      padding: 5,
    },
    itemImage: {
      height:300,
      width:300,
      resizeMode:"contain",
    },
    itemName: {
      fontSize:15,
      fontWeight: 'bold'
    },
    itemPrice: {
      fontSize:20,
      fontWeight: 'bold',
      color:'blue'
    },
    itemDesc: {
    width: 370,
    backgroundColor: '#EFEFEF',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    margin:10,
    paddingTop:5
    
    },
    itemButton: {
  
    },
    buttonText: {
      
    },
  });
  