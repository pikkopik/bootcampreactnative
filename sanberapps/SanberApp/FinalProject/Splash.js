import React, { useEffect } from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Login")
        }, 2000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1,backgroundColor: '#4A0004'}}>
             </View>
    )
}

export default Splash

const styles = StyleSheet.create({})
