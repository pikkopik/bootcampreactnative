import React from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';

export default class LoginScreen extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    var pw = this.state.password;
    var nama = this.state.userName;
    // console.log(pw, nama);  
      const user = {
      username: nama,
      password: pw,
    };
    
    if(pw=='' || nama==''){
        alert(`Mohon Lengkapi Data`);
    }else    
    {

      if (pw != 12345678) {
        alert('Password Salah!');
      } else {
        alert(`Selamat Datang: ${nama}!`);
        this.props.navigation.navigate('Home', {key: {nama}});
      }
      
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.titleText}>Soal Quiz 3</Text>
          <Text style={styles.subTitleText}>Sanbercode</Text>
        </View>

        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Username/Email</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Nama User/Email'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
          <Button title='Login' onPress={() => this.loginHandler()} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 48,
    fontWeight: 'bold',
    color: 'blue',
    textAlign: 'center',
  },
  subTitleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'blue',
    alignSelf: 'flex-end',
    marginBottom: 16
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold'
  },
  textInput: {
    width: 300,
    backgroundColor: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
