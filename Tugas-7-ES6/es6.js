console.log(' ')
console.log('=============== Soal 1-Mengubah Fungsi Menjadi Arrow ===============')
console.log(' ')
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}

golden()



console.log(' ')
console.log('=============== Soal 2-Sederhanakan menjadi Object literal di ES6 ===============')
console.log(' ')
const newFunction = literal = (firstName, lastName) => {
    return {
        firstName, lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()



console.log(' ')
console.log('=============== Soal 3-Destructuring ==============')
console.log(' ')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}


/*const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;*/

//Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)

console.log(' ')
console.log('=============== Soal 4-Array Spreading ===============')
console.log(' ')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log(' ')
console.log('=============== Soal 5-Template Literals ===============')
console.log(' ')
const planet = "earth"
const view = "glass"
/*var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'*/

const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor  
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(after)
console.log(' ')


